﻿using NLog;
using System.Collections.Generic;
using System.Linq;

namespace Connector.Connector{
    public class ConnectorService{
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private static ConnectorService Service = null;

        /// <summary>
        /// 讀取設定檔並產生<c>ConnectorService</c>，
        /// 內容包含各個設定對應的<c>IConnector</c>
        /// </summary>
        /// <param name="configuration">設定檔</param>
        public static void Init(Configuration configuration){
            if (Service != null) { return; }
            Service = new ConnectorService(configuration);
        }

        /// <summary>
        /// 用
        /// </summary>
        /// <param name="connectorName"></param>
        /// <returns></returns>
        public static IConnector GetConnector(string connectorName){
            return Service.Connectors[connectorName];
        }

        public static List<IConnector> GetAllConnector(){
            return Service.Connectors.Values.ToList();
        }

        private readonly Dictionary<string, IConnector> Connectors;
        /// <summary>
        /// <para>驗證設定是否正確並建立對應的<c>FileSender</c>或<c>FileReceiver</c>，
        /// 若有錯誤則不會建立對應的Class</para>
        /// <para><c>FileSender</c>，connector id規則:Output_{config.DocType}_{config.ToDuns}</para>
        /// <para><c>FileReceiver</c>，connector id固定為Input</para>
        /// </summary>
        /// <param name="configuration">設定檔</param>
        ConnectorService(Configuration configuration){
            Connectors = new Dictionary<string, IConnector>();
            if (!ValidSender(configuration.Sender)) return;

            //[[Output]]#送件目標  為多筆
            foreach (OutputConfig config in configuration.Output){
                if (!ValidOutput(config)) continue;

                var id = "Output_" + config.DocType + "_" + config.ToDuns;
                Connectors.Add(id, new FileSender(configuration.Sender, config, id));
            }
            //[Input]#接收 API gateWay File 單筆
            if (!ValidInput(configuration.Input)) return;
            Connectors.Add("Input", new FileReceiver(configuration.Sender, configuration.Input, "Input"));
        }

        /// <summary>
        /// 驗證是否有填值，若無則紀錄Log
        /// </summary>
        /// <param name="propertyName">紀錄Log時提示的名字</param>
        /// <param name="property">用來驗證的參數</param>
        /// <returns>驗證通過則回傳true，不通過則false</returns>
        private bool ValidAndLog(string propertyName, string property) {
            if (!string.IsNullOrWhiteSpace(property))
                return true;
            logger.Error(propertyName + " is empty.");
            return false;
        }

        /// <summary>
        /// 驗證Sender的Config
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        private bool ValidSender(SenderConfig sender){
            logger.Info("Valid Sender Configuration.");
            bool result = true;
            result = result && ValidAndLog("AwsApiKey", sender.AwsApiKey);
            result = result && ValidAndLog("CheckKey", sender.CheckKey);
            result = result && ValidAndLog("CompanyDuns", sender.CompanyDuns);
            result = result && ValidAndLog("InputUrl", sender.InputUrl);
            result = result && ValidAndLog("OutputUrl", sender.OutputUrl);
            return result;
        }

        /// <summary>
        /// 驗證Output的Config
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        private bool ValidOutput(OutputConfig output){
            bool result = true;
            result = result && ValidAndLog("FolderPath", output.FolderPath);
            result = result && ValidAndLog("Extension", output.Extension);
            result = result && ValidAndLog("ArchiveFolder", output.ArchiveFolder);
            result = result && ValidAndLog("ToDuns", output.ToDuns);
            result = result && ValidAndLog("DocType", output.DocType);
            return result;
        }

        /// <summary>
        /// 驗證Input的Config
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ValidInput(InputConfig input){
            bool result = true;
            result = result && ValidAndLog("FolderPath", input.FolderPath);
            result = result && ValidAndLog("ApiFrom", input.ApiFrom);
            return result;
        }
    }

    public interface IConnector{
        string Id { get; }
        string CronExp { get; }
        void Notify();
    }

}
