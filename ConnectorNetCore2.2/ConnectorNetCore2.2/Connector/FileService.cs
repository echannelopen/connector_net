﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;
using System.Threading;
using Connector.Connector;
using ConnectorNetCore2;
using NLog;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



namespace Connector{
    public class FileSender : IConnector{
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public string CronExp { get { return Sender.CronExpression; } }
        public string Id { get; }
        private readonly SenderConfig Sender;
        private readonly OutputConfig Output;
        private bool Locked = false;
        public FileSender(SenderConfig Sender, OutputConfig Output, string Id){
            this.Sender = Sender;
            this.Output = Output;
            this.Id = Id;
        }
        /// <summary>
        /// 1. 先檢查Output.Folder是否存在，若不存在會記錄一筆log並退出
        /// 2. 取得Folder內的所有檔案(會依照extension過濾)
        /// 3. 分別檢查檔案是否存在，存在則回傳出去，不存在則記錄一筆Log並跳過
        /// </summary>
        /// <returns>file</returns>
        private IEnumerable<FileInfo> GetPreparedFiles(){
            var outFolder = new DirectoryInfo(Output.FolderPath);
            if (!outFolder.Exists){
                logger.Info(Output.FolderPath + " is not existence.");
                yield break;
            }

            foreach (FileInfo file in outFolder.GetFiles("*" + Output.Extension)){
                if (!file.Exists){
                    logger.Info(file.FullName + " is not existence.");
                    continue;
                }
                yield return file;
            }
        }
        /// <summary>
        /// 建立Multipart/form-data形式的request body
        /// </summary>
        /// <param name="file">此次要送出的檔案，會被轉成Bytes</param>
        /// <returns></returns>
        private MultipartFormDataContent BuildForm(FileInfo file){
            var form = new MultipartFormDataContent();
            var content = new ByteArrayContent(File.ReadAllBytes(file.FullName));
            content.Headers.ContentDisposition =
                new ContentDispositionHeaderValue("form-data"){
                    Name = "data",
                    FileName = file.Name
                };
            form.Add(content, "file");
            form.Add(new StringContent(Sender.CompanyDuns), "fromDuns");
            form.Add(new StringContent(Output.ToDuns), "toDuns");
            form.Add(new StringContent(Output.DocType), "docType");
            form.Add(new StringContent(file.Name), "docKey");
            form.Add(new StringContent(Sender.CheckKey), "checkKey");
            form.Headers.Add("x-api-key", Sender.AwsApiKey);
            //form.Headers.Add("check-key", Sender.CheckKey);
            return form;
        }

        /// <summary>
        /// 將成功送出的資料移到備份的資料夾
        /// 備份檔案會修改名稱，檔名增加_YYYYMMDDmmss
        /// </summary>
        /// <param name="file"></param>
        private void BackupFile(FileInfo file){
            String sSystime = DateTime.Now.ToString("yyyyMMddmmss");
            var dateFolder = DateTime.Now.ToString("yyyy/MM/dd");
            var folderpath = Path.Join(Output.ArchiveFolder, dateFolder);
            Directory.CreateDirectory(folderpath);
            var filepath = Path.Join(folderpath, sSystime + "_" + file.Name  );
            if (File.Exists(filepath)){
                logger.Error("there is file at " + filepath);
                return;
            }
            file.MoveTo(filepath);
            logger.Info("Move file:" + sSystime + "_" + file.Name + " to " + filepath);
            return;
        }

        /// <summary>
        /// 遍歷所有的files，透過http request送出，若成功則備份文件
        /// </summary>
        private async void SendFile(){
            using (HttpClient client = new HttpClient()){
                Uri uri = new Uri(Sender.OutputUrl);
                foreach (FileInfo file in GetPreparedFiles()){
                    logger.Info("send file:" + file.Name + " to connector.");
                    var response = await client.PostAsync(uri, BuildForm(file));
                    logger.Info(">>>>SendFile_response=" + response.ToString());
                    logger.Info(">>>>SendFile_RequestMessage=" + response.RequestMessage);
                    if (response.IsSuccessStatusCode){
                        BackupFile(file);
                    }
                }
            }
            return;
        }
        private void Execute(){
            lock (this){
                if (Locked) return;
                Locked = true;
            }try{
                logger.Info("start send file.");
                SendFile();
            }catch (Exception e){
                logger.Error(e);
            }

            Locked = false;
            return;
        }
        public void Notify(){
            Execute();
            return;
        }
    }

    public class FileReceiver : IConnector{
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public string CronExp { get { return Sender.CronExpression; } }
        public string Id { get; }
        private readonly SenderConfig Sender;
        private readonly InputConfig Input;

        private bool Locked = false;
        public FileReceiver(SenderConfig Sender, InputConfig Input, string Id){
            this.Sender = Sender;
            this.Input = Input;
            this.Id = Id;
        }
        
        /// <summary>
        /// 透過http request取得檔案並儲存
        /// </summary>
        private async void GetFile(){
            using (var client = new HttpClient()){
                String sUrl = Sender.InputUrl+"?duns="+Sender.CompanyDuns+"&checkKey="+Sender.CheckKey;
                var vUri = new Uri(sUrl);
                client.DefaultRequestHeaders.Add("x-api-key", Sender.AwsApiKey);
                client.DefaultRequestHeaders.Add("check-key", Sender.CheckKey);
                client.DefaultRequestHeaders.Add("x-api-from", Input.ApiFrom);
                client.DefaultRequestHeaders.Add("duns", Sender.CompanyDuns);
                try{
                    var response = await client.GetAsync(vUri);
                    logger.Info(">>>>getFile_response=\n" + response.ToString());
                    logger.Info(">>>>getFile_RequestMessage=\n" + response.RequestMessage);

                    if (response.StatusCode != HttpStatusCode.OK){
                        logger.Debug(response.RequestMessage);
                        return;
                    }else{
                        Download(response);
                    }
                }
                catch (Exception e){
                    logger.Info(e.StackTrace);
                }
            }
            return;
        }
        /// <summary>
        /// 將response的內容複製到Input.Folder
        /// </summary>
        /// <param name="response"></param>
        private async void Download(HttpResponseMessage response){        
            string sResult = await response.Content.ReadAsStringAsync();
            logger.Info(">>>>getFile_Result=\n" + sResult);
            var details = JObject.Parse(sResult);    
            if ( details["sqsMessage"] == null){      
                 logger.Info("sqsMessage content is null.");
                return;
            }  
              
            String sFileContent =  details["sqsMessage"]["fileContent"].ToString();
            if (sFileContent == null){
                logger.Info("file content is null.");
                return;
            } else if (response.Content == null){
                logger.Info("response content is null.");
                return;
            }
            
            //sFullPath={inFolder}/{from_duns}/{doc_type}
            String sPathFromDuns = details["sqsMessage"]["info"]["fromDuns"].ToString();
            String sPathDocType = details["sqsMessage"]["info"]["docType"].ToString();
            String sFilePath = details["sqsMessage"]["filePath"].ToString();
            String sFileName = Path.GetFileName(sFilePath);

            String sFullPath = Input.FolderPath+"/"+sPathFromDuns+"/"+sPathDocType;

            //create directory
            System.IO.Directory.CreateDirectory(sFullPath);
            logger.Info("CreateDirectory="+sFullPath);

            //create file write Stream
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(sFileContent);
            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            System.IO.Stream dataStream = httpResponse.GetResponseStream();
            byte[] buffer = new byte[8192];

            FileStream fs = new FileStream(sFullPath+"/"+ sFileName, FileMode.Create, FileAccess.Write);
            int size = 0;
            do{
                size = dataStream.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    fs.Write(buffer, 0, size);
            } while (size > 0);
            fs.Close();                
            httpResponse.Close();

            logger.Info("start Delete file.");
            DeleteFile(Sender.CompanyDuns ,Sender.CheckKey
            ,details["sqsMessage"]["messageId"].ToString()
            ,details["sqsMessage"]["receiptHandle"].ToString());
            return;
        }
         private async void DeleteFile(String sCompanyDuns,String sCheckKey,String sMessageId,String sReceiptHandle){
            using (var client = new HttpClient()){
                var uri = new Uri(Sender.InputUrl);//+"/inbox"
                client.DefaultRequestHeaders.Add("x-api-key", Sender.AwsApiKey);
                client.DefaultRequestHeaders.Add("check-key", Sender.CheckKey);
                client.DefaultRequestHeaders.Add("x-api-from", Input.ApiFrom);
                client.DefaultRequestHeaders.Add("duns", sCompanyDuns);
                
                //Body
                String sBody = "{\"duns\":\""+sCompanyDuns+"\",\"checkKey\":\""+sCheckKey+"\",\"messageId\":\""+sMessageId+"\",\"receiptHandle\":\""+sReceiptHandle+"\"}";

                try{                   
                    var request = new HttpRequestMessage {
                        Method = HttpMethod.Delete,
                        RequestUri = new Uri(Sender.InputUrl),
                        Content = new StringContent(sBody, SenderConfig.UTF8, "application/json")
                    };
                    var response = await client.SendAsync(request);

                    logger.Info(">>>>deleteFile_response=\n" + response.ToString());
                    logger.Info(">>>>deleteFile_RequestMessage=\n" + response.RequestMessage);

                    if (response.StatusCode != HttpStatusCode.OK){
                        logger.Debug(response.RequestMessage);
                        return;
                    }else{
                         logger.Info(">>>>deleteFile =" + response.StatusCode);
                    }
                }catch (Exception e){
                    logger.Error(e);
                }
            }
            // return;
        }

        public void Execute(){
            lock (this){
                if (Locked) return;
                Locked = true;
            }try{
                logger.Info("start get file.");
                GetFile();                
            }catch (Exception e){
                logger.Error(e);
            }

            Locked = false;
            return;
        }
        public void Notify(){
            Execute();
            return;
        }
    }
}