﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connector{
    public class Configuration{
        public string LogFolder { get; set; }
        public SenderConfig Sender { get; set; }
        public List<OutputConfig> Output { get; set; }
        public InputConfig Input { get; set; }
    }

    public class SenderConfig {
        public string CompanyDuns { get; set; }
        public string CheckKey { get; set; }
        public string AwsApiKey { get; set; }
        public string OutputUrl { get; set; }
        public string InputUrl { get; set; }
        public string CronExpression { get; set; }
        public static System.Text.Encoding UTF8 { get; }
    }

    public class OutputConfig {
        public string FolderPath { get; set; }
        public string Extension { get; set; }
        public string ArchiveFolder { get; set; }
        public string ToDuns { get; set; }
        public string DocType { get; set; }
        
    }

    public class InputConfig {
        public string FolderPath { get; set; }
        public string ApiFrom { get; set; }
    }
}
