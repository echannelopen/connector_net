﻿using Connector;
using Connector.Connector;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConnectorNetCore2{
    class Scheduler : IHostedService    {
        private IScheduler scheduler;
        public async Task StartAsync(CancellationToken cancellationToken){
            await BuildSchedule();
        }
        /// <summary>
        /// 從<c>ConnectorService</c>取得所有的Connector資訊建立JOB
        /// </summary>
        private async Task BuildSchedule() {
            var connectors = ConnectorService.GetAllConnector();
            var factory = new StdSchedulerFactory();

            scheduler = await factory.GetScheduler();
            foreach (IConnector connector in connectors){
                var jobDetail = JobBuilder.Create<ConnectorTask>().UsingJobData("connector_id", connector.Id).Build();
                var trigger = TriggerBuilder.Create().StartNow().WithCronSchedule(connector.CronExp).Build();
                await scheduler.ScheduleJob(jobDetail,trigger);
            }
            await scheduler.Start();
        }

        public async Task StopAsync(CancellationToken cancellationToken){
            if (scheduler == null)
                return;
            await scheduler.Shutdown(waitForJobsToComplete: true);
        }
    }
}
