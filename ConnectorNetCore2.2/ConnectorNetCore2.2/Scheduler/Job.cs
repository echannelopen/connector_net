﻿using Connector.Connector;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connector{
    public class ConnectorTask : IJob{
        ///<summary>
        /// 用 <c>connector_id</c> 通知 <c>ConnectorService</c> 要執行的JOB
        ///</summary>
        public Task Execute(IJobExecutionContext context){
            string id = (string)context.JobDetail.JobDataMap["connector_id"];
            IConnector connector = ConnectorService.GetConnector(id);
            return Task.Run(() => connector?.Notify());
        }
    }
}
