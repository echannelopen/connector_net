﻿using Connector;
using Connector.Connector;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nett;
using NLog;
using Quartz;
using Quartz.Impl;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConnectorNetCore2._2{
    class Program{
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// 初始建置流程
        /// <para>1. 讀取並解析Config</para>
        /// <para>2. 在<c>ConnectorService</c>中依照Config建立對定的<c>IConnector</c></para>
        /// <para>3. 從<c>ConnectorService</c>中取得<c>IConnector</c>並建立Job</para>
        /// <para>4. 啟動Hosting Server</para>
        /// </summary>
        static void Main(){
            logger.Info("system start");

            // ------Visual Studio Path-------
            //string sPath = @"..\..\..\conf\config.toml";
            // -------Visual Studio Path-------

            // ------VSCode Path-------
            string sPath = System.IO.Directory.GetCurrentDirectory();
            sPath = sPath + "\\conf\\config.toml";
            // ------VSCode Path-------

            logger.Info("sPath="+sPath);
            if (System.IO.File.Exists(sPath))
            {
                Configuration conf = Toml.ReadFile<Configuration>(sPath);
                ConnectorService.Init(conf);
                BuildSchedulerAndStartHosting();
            }
            else {
                logger.Info("Could not find a part of the path :"+ sPath);
            }
            logger.Info("system end");
        }

        static void BuildSchedulerAndStartHosting(){
            var host = new HostBuilder()
                .ConfigureServices((service) => service.AddHostedService<Scheduler>())
                .UseConsoleLifetime()
                .Build();
            host.Start();
            host.WaitForShutdown();
            host.Dispose();
        }
    }
}
