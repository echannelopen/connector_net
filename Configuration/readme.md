##  功能  ##
* 將指令目錄檔案透過 REST 送至 Connector
  * 需區分給 WPG or 其他 Vendor
* 透過 REST 取得資料
  * 取得之後須依 Vendor & Doc Type 區分目錄
* Job
  * in
    * 抓取檔案複製指定目錄
  * out 
    * 抓取指定目錄檔案送出 (多個目錄對多個對象)
* 跨平台執行
* 目錄 / API Key 等可以讀取 config 檔
  * 預計使用 toml 格式  
  * 送出/接收 時需知道 from/ to / doc_type 

## 預計開發工具 ## 
* Java  
    * spring boot  ?
      * job
      * 可以不需要 tomcat 內建 AP 
* .Net Core ?
* Python ?
    * 搭配 bat or sh 檔?
  

## Issue ###
* 營運
  * Log
    * TX Log 
      * filename \ sqs messageid
      * 執行的 log (error ...)
  * 監控
    * 監控 get api key 使用狀況? 還是 lambda 動手統計最後讀取時機?
  * 檔案清理 
    * 新增 Job 清理檔案?

```
#Sender Company Information
from_company = "123456789"
check_key = "xxxxxxsfsdfasdf"
x-api-key = "XXXXXXXXOXOXOXOXX"
logpath = "D:\test"
output_url = ""
input_url = ""
[[output]]
forder = "C:\OutPath\656127214"  # 預計送出的檔案
extension = ".xlst"  # 只處理這類附檔名
archive_forder = "C:\OutPath\656127214\archive" # 已送出的檔案
to_duns = "656127214"  # WPG
doc_type = "Forecast"

[[output]]
forder = "C:\OutPath"  # 預計送出的檔案
extension = ".xlst"
archive_forder = "C:\OutPath\656127214\archive" # 已送出的檔案
to_duns = "656127214"  # WPG
doc_type = "VMI"

[inout]
# 預計寫入的檔案 目錄結構 C:\Input\from_duns\doc_type
forder = "C:\Input"  
```