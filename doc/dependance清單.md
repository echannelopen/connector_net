# Dependance清單

## NuGet管理套件

- Nett: 將toml config轉換成class
- NLog: 紀錄Log
- Quartz: 建立 Scheduler Job

※NLog說明，用NuGet安裝的NLog.Config會自動產生共用的NLog.config，
  但這個config在建置期間並不會複製到相對應的Debug或Release資料夾，
  會造成執行期間讀不到config的狀況，
  
  解決辦法:
  手動複製一份設定檔->設定檔放置在根目錄->選擇檔案屬性->建置動作的值改為: `內容`

__無法正確執行的配置:__

![nlog_link](./NLog_Config_link.png)

__可以正確執行的配置:__

![nlog_link](./NLog_Config.png)